import { FETCH_SUCESS_LOGIN,FETCH_ERROR_LOGIN } from '../actions/login'

const initialLoginState = 
{
    success:false,
    data:{}
}
 const loginAutheticate = (state = initialLoginState,action) => {
    switch (action.type) {
        //FETCH_LOGIN - esta sendo observado pelo saga
        case FETCH_SUCESS_LOGIN:
            window.location.hash = '#/home'
            return{
                ...state,
                success:true,
                data: action.payload
            }
        case FETCH_ERROR_LOGIN:
            return{
                ...state,
                success:false,
                data: action.payload
            }
        default:
            return state
    }
}

export default loginAutheticate;