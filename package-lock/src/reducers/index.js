import { combineReducers } from "redux";
import loginAutheticate from './login';
import loadModal from './load-modal';
import InfoModalOpen from './info-modal';
import LoadHomeEnterprise from './home';

const rootReducers = combineReducers({
    load: loadModal,
    login: loginAutheticate,
    infoModal: InfoModalOpen,
    enterprises: LoadHomeEnterprise
});

export default rootReducers;