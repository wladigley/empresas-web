import { RESULT_ENTERPRISE } from '../actions/home'

const initialHomeState =
{
    enterprises: []
}
const LoadHomeEnterprise = (state = initialHomeState, action) => {
    switch (action.type) {
        case RESULT_ENTERPRISE:
            debugger
            return {
                enterprises: action.payload
            }
        default:
            return state
    }
}

export default LoadHomeEnterprise;