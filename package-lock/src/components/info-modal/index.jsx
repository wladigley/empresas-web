import 'materialize-css';
import React from 'react';
import { connect } from 'react-redux'
import { Modal, Button } from 'react-materialize';

function InfoModal(props) {
    return (
        <div>
            <Modal
                actions={[
                    <Button flat modal="close" node="button" waves="green">Close</Button>
                ]}
                bottomSheet={false}
                fixedFooter={false}
                // header="Modal Header"
                id="Modal-1"
                open={props.open}
                options={{
                    dismissible: true,
                    endingTop: '10%',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    opacity: 0.5,
                    outDuration: 250,
                    preventScrolling: true,
                    startingTop: '4%'
                }}
            >
                <p>
                    {props.msg}
                </p>
            </Modal>
        </div>
    )
}

const mapStateToProps = (state) => ({
    open: state.infoModal.show,
    msg: state.infoModal.txt
})

export default connect(mapStateToProps, null)(InfoModal)