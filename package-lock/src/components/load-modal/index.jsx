import React from 'react'
import {connect} from 'react-redux'
import 'materialize-css';
import {  Modal, Preloader } from 'react-materialize';

function LoadModal(props) {
    return (
        <div>
            <Modal
              actions={[
              ]}
                id="Modal-0"
                open={props.load.loading}
                options={{
                    dismissible: false,
                    endingTop: '10%',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    opacity: 0.5,
                    outDuration: 250,
                    preventScrolling: true,
                    startingTop: '4%'
                }}
            >
                <div className="center-align">
                    <Preloader
                        active
                        color="blue"
                        flashing={false}
                        size="big"
                    />
                </div>


            </Modal>
        </div>
    )
}

const mapStateToProps = (state) => ({
    load: state.load
})

export default connect(mapStateToProps, null)(LoadModal)
