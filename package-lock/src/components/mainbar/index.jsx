import React, { useState } from 'react';
import { Navbar, NavItem, Icon } from 'react-materialize';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import * as search from '../../actions/search';

function MainBar(props) {

    const [state,setState] = useState(false);
    
    const setSearchBar = () => {
        setState(!state)
    }

    const keyDownSearchBar = (event) =>{
        if(event.keyCode === 27) {
            setState(false)
        }else if(event.keyCode === 13){
            let text = event.target.value;
            props.exeuteSearch(text);
        }
    }
    return (
        <div>
            <Navbar
                className="focus active"
                alignLinks="right"
                brand={
                    <div className="brand-logo">
                        <img  className="logo_nav" alt="logo" src={process.env.PUBLIC_URL + "/img/home/logo-nav.png"} srcSet={process.env.PUBLIC_URL + "/img/home/logo-nav@2x.png 2x," + process.env.PUBLIC_URL + "/img/home/logo-nav@3x.png 3x"} ></img>
                    </div>

                }
                centerLogo
                id="mobile-nav"
                menuIcon={<Icon>menu</Icon>}                
                options={{
                    draggable: true,
                    edge: 'left',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    outDuration: 200,
                    preventScrolling: true
                }}
                onKeyDown = {(e) => keyDownSearchBar(e)}
                search = {state}
                tooltip="I am a tooltip"
            >
                <NavItem onClick={()=>setSearchBar()}>
                    <Icon >
                        search
                    </Icon>

                </NavItem>
            </Navbar>            
        </div>
    )
}

const mapDispatchToProps = dispatch => bindActionCreators(search, dispatch)

export default connect(null, mapDispatchToProps)(MainBar);