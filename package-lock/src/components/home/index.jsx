import 'materialize-css';
import React from 'react';
import { useSelector } from 'react-redux'
import MainBar from '../mainbar'
import { Row, Col, Card, Icon, CardTitle } from 'react-materialize';

function Home() {

    const enterprises = useSelector(state => state.enterprises.enterprises)

    console.log(enterprises)

    debugger

    return (
        <div>
            <MainBar />



            {enterprises.enterprises?
                enterprises.enterprises.map((itemmap) => (
                    itemmap.enterprises.map((item, index)=>(
                        <Row key={index}>
                        <Col
                            m={6}
                            s={12}
                        >
                            <Card
                                actions={[
                                    // eslint-disable-next-line
                                    <a key="1" href="#">This is a link</a>
                                ]}
                                closeIcon={<Icon>close</Icon>}
                                header={<CardTitle image={item.photo} />}
                                horizontal
                                title={item.enterprise_name}
                                revealIcon={<Icon>more_vert</Icon>}
                            >
                                title={item.description}
                            </Card>
                        </Col>
                    </Row>
                ))

                )) : (
                    <div className="center-align container">
                        <p className="Clique-na-busca-para center-align container">
                            Clique na busca para iniciar.
                        </p>
                    </div>
                )
            }
        </div>
    )
}


export default Home