import {HashRouter as Router, Route, Switch} from 'react-router-dom';
import routes from './components/routes';
import { Provider } from "react-redux";
import store from './redux/store';
import React from 'react';
import LoadModal from './components/load-modal';
import InfoModal from './components/info-modal';

function App() {
    return (
        <div>
            <Provider store={store}>
                <Router>
                    <Switch>
                    {routes.map((route,index)=>(
                    <Route key={index} path={route.uri} component={route.component} exact></Route>))}
                    <Route component={()=>(<div>Not Found!</div>)}></Route>
                    </Switch>
                </Router>
                <LoadModal/>
                <InfoModal/>
            </Provider>
        </div>
    )
}

export default App
