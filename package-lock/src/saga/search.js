import { takeEvery, put, select } from 'redux-saga/effects';
import { getToken } from '../redux/selectors';
import axios from 'axios';
import { LOAD_MODAL } from '../actions/load-modal';
import { URL_API_ENTERPRISE } from '../actions/comun';
import { LOAD_INFO_MODAL} from '../actions/info-modal';
import { FETCH_SEARCH} from '../actions/search';
import { RESULT_ENTERPRISE } from '../actions/home';


function* fetchSearch(action) {
    let token = yield select(getToken);
    //const requestData = action.payload; // Recupera as informações da pesquisa pelo payload

    //falta Fazer condição para cada tipo de pesquisa vindo da search bar
    // const all_uri = '/enterprises'
    // const single_enterprises_uri = '/enterprises/1'
    // const dinamic_enterprises_uri = '/enterprises?enterprise_types=1&name=aQm'

    let result = {
        success: false,
        error: {},
        data: {}
    }

    let headers = {
        'Content-Type': 'application/json',
        'access-token': token.AccessToken,
        'client': token.client,
        'uid': token.uid
    }
    // const jsonHeaders = JSON.stringify(json);

    yield put({ type: LOAD_MODAL,payload:true });;

    axios.interceptors.request.use(function (config) {

        config.headers =  headers;
    
        return config;
    });
    debugger
    yield axios.get(URL_API_ENTERPRISE+'/enterprises')
        .then(response => {
            debugger
            if (response.status  === 200) {
                result.success = true;
                result.data = response.data;
            }
            else {
                result.success = false;
                result.error = {};
            }
        })
        .catch(error => {
            debugger
            result.success = false;
            result.error = error;
        });

    yield put({ type: LOAD_MODAL,payload:false });;
    debugger
    if (result.success) {
        yield put({ type: RESULT_ENTERPRISE, payload: {enterprises: result.data.enterprises} });
    } else {
        yield put({ type: LOAD_INFO_MODAL,payload:{  show: true, txt: 'Falha ao retornar pesquisa, tente novamente.'}});
    }

}

export default function* listnerSearch() {
    yield takeEvery(FETCH_SEARCH, fetchSearch);
}