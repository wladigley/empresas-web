export const FETCH_SEARCH = 'SEARCH';
export const FETCH_SUCCESS_SEARCH = 'SEARCH';
export const FETCH_ERROR_SEARCH = 'SEARCH';

export function exeuteSearch(text) {
    return { type: FETCH_SEARCH, payload: text }
}