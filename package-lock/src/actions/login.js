export const FETCH_LOGIN = 'FETCH_LOGIN';
export const FETCH_SUCESS_LOGIN = 'FETCH_SUCESS_LOGIN';
export const FETCH_ERROR_LOGIN = 'FETCH_ERROR_LOGIN';

export function requestLogin(login) {
    return { type: FETCH_LOGIN, payload: login }
}
