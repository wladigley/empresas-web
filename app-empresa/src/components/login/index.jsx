import React,{useState} from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import * as allAcionsLogin from '../../actions/login';

function Login(props) {
    const login = {
        email: '',
        password: ''
    }
    const [state, setstate] = useState(login)

    const startLogin = () => {
        props.requestLogin(state);
        setstate(login)
    }

    //Feito utilizando tags element para, para demonstrar habilidade com html
    return (
        <>
        <div className = "center-align container">
        <form className="login-page">
            <div className="col s12">
                <img className="logo_home col s12" alt="logo" src={process.env.PUBLIC_URL + "/img/login/logo-home.png"} srcSet={process.env.PUBLIC_URL + "/img/login/logo-home@2x.png 2x," + process.env.PUBLIC_URL + "/img/login/logo-home@3x.png 3x"} ></img>
            </div>
            <div className="row">
                <div className="col s4"></div>
                <div className="col s4">
                    <div className="BEM-VINDO-AO-EMPRESA">
                        <p>BEM-VINDO AO EMPRESAS</p>
                    </div>
                </div>
                <div className="col s4"></div>
            </div>

            <div className="row">
                <div className="col s2"></div>
                <div className="col s8">
                    <div className="Lorem-ipsum-dolor-si">
                        <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
                    </div>
                </div>
                <div className="col s2"></div>
            </div>

            <div className="row">
                <div className="col s4"></div>
                <div className="col s4">
                    <div className="input-field">
                        <i className="material-icons-outlined prefix">mail</i>
                        <input
                            onChange={(e) => { setstate({ ...state, email: e.target.value }) }}
                            value={state.email}
                            placeholder="Email"
                            id="email" type="email"
                            className="E-mail">
                        </input>
                    </div>

                    <div className="input-field">
                        <i className="material-icons-outlined prefix">https</i>
                        <input
                            onChange={(e) => { setstate({ ...state, password: e.target.value }) }}
                            value={state.password}
                            placeholder="password"
                            id="password"
                            type="password"
                            className="Senha">
                        </input>
                    </div>

                </div>
                <div className="col s4"></div>
            </div>
            <button type="submit" onClick={() => startLogin()} className="Rectangle-26 waves-effect waves-light btn-large"> Entrar</button>
        </form>
        </div>
        </>
    )
}
//utilizando recurso de paradigma class componente de proposito
const mapDispatchToProps = dispatch => bindActionCreators(allAcionsLogin, dispatch)

export default connect(null, mapDispatchToProps)(Login);
