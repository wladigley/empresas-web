import Login from './login';
import Home from './home';
import Detail from './detail';

export default [
    {
        name:'Login',
        component: Login,
        uri:'/'
    },    
    {
        name:'Login',
        component: Login,
        uri:'/Login'
    },
    {
        name:'Home',
        component: Home,
        uri:'/Home'
    },
    {
        name:'Detail',
        component: Detail,
        uri:'/Detail'
    }
]