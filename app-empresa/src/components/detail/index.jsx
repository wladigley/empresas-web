import React, { useEffect } from 'react';
import { useSelector } from 'react-redux'
import MainBar from '../mainbar';
import { URL_API_ENTERPRISE } from '../../actions/comun';
import { Row, Col, Card, Icon, CardTitle } from 'react-materialize';

function Detail() {
    
    const login = useSelector(state => state.login)
    
    const enterprises = useSelector(state => state.detail.enterprises.enterprise)

    console.log(enterprises)

    console.log(enterprises.enterprise_name)

    useEffect(()=>{
        if (!login.success) {
            window.location.hash = '#/'
        }
    // eslint-disable-next-line 
    },[])   
    return (
        <div>
            <MainBar />
            <Row >
                <Col
                    m={12}
                    s={12}
                >
                    <Card

                        closeIcon={<Icon>close</Icon>}
                        header={<CardTitle image={URL_API_ENTERPRISE + enterprises.photo} />}
                        horizontal
                        title={enterprises.enterprise_name}
                        revealIcon={<Icon>more_vert</Icon>}
                    >
                        {enterprises.description}
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Detail
