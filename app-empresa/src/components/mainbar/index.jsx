import React, { useState } from 'react';
import { Navbar, NavItem, Icon, Button } from 'react-materialize';
import { useDispatch } from 'react-redux'
import * as search from '../../actions/search';
import * as home from '../../actions/home';

function MainBar() {

    //utilizando hooks 
    const [state, setState] = useState(false);
    const dispatch = useDispatch()

    const setSearchBar = () => {
        setState(!state)
    }

    const keyDownSearchBar = (event) => {
        if (event.keyCode === 27) {
            setState(false)
        } else if (event.keyCode === 13) {
            let text = event.target.value;

            dispatch(search.exeuteSearch({ data: text, singleResult: true, actionSuccesResult: home.RESULT_ENTERPRISE }));
        }
    }

    const getAllEnterprises = () => {
        dispatch(search.exeuteSearch({ data: '', allResult: true, actionSuccesResult: home.RESULT_ENTERPRISE }));
    }
    return (
        <div>
            <Navbar
                className="focus active"
                alignLinks="right"
                brand={
                    <div className="brand-logo">
                        <img className="logo_nav" alt="logo" src={process.env.PUBLIC_URL + "/img/home/logo-nav.png"} srcSet={process.env.PUBLIC_URL + "/img/home/logo-nav@2x.png 2x," + process.env.PUBLIC_URL + "/img/home/logo-nav@3x.png 3x"} ></img>
                    </div>

                }
                centerLogo
                id="mobile-nav"
                menuIcon={<Icon>menu</Icon>}
                options={{
                    draggable: true,
                    edge: 'left',
                    inDuration: 250,
                    onCloseEnd: null,
                    onCloseStart: null,
                    onOpenEnd: null,
                    onOpenStart: null,
                    outDuration: 200,
                    preventScrolling: true
                }}
                onKeyDown={(e) => keyDownSearchBar(e)}
                search={state}
                tooltip="I am a tooltip"
            >
                <NavItem onClick={() => setSearchBar()}>
                    <Icon >
                        search
                    </Icon>

                </NavItem>

                <Button
                    onClick={() => getAllEnterprises()}
                    className="nav nav-item nav-wrapper col l2 offset-l1 offset-s4 s4"
                    node="button"
                    tooltip="All Enterprises"
                    tooltipOptions={{
                        position: 'bottom'
                    }}
                    waves="light"
                >
                <Icon >
                    cloud_download
                </Icon>
            </Button>
            </Navbar>
        </div>
    )
}

export default MainBar;