import 'materialize-css';
import MainBar from '../mainbar'
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { URL_API_ENTERPRISE } from '../../actions/comun';
import  * as detail from '../../actions/detail';
import * as search from '../../actions/search';
import { Row, Col, Card, Icon, CardTitle } from 'react-materialize';

function Home() {

    const enterprises = useSelector(state => state.enterprises.enterprises)
    const login = useSelector(state => state.login)
    const dispatch = useDispatch()
    useEffect(()=>{
        if (!login.success) {
            window.location.hash = '#/'
        }
    // eslint-disable-next-line 
    },[])

    const getDetailEnterprise = id =>{
        dispatch(search.exeuteSearch({ data: id, singleResult: true, actionSuccesResult: detail.FETCH_SUCCESS_DETAIL }));
    }
    return (
        <div>
            <MainBar />
            <div className="container">
                {enterprises.enterprises ?
                    enterprises.enterprises.map((item, index) => (
                        <Row key={index}>
                            <Col
                                m={12}
                                s={12}
                            >
                                <Card
                                    actions={[
                                        // eslint-disable-next-line
                                        <button key="1" onClick={()=>getDetailEnterprise(item.id)}>This is a link</button>
                                    ]}
                                    closeIcon={<Icon>close</Icon>}
                                    header={<CardTitle image={URL_API_ENTERPRISE + item.photo} />}
                                    horizontal
                                    title={item.enterprise_name}
                                    revealIcon={<Icon>more_vert</Icon>}
                                >
                                    {item.description}
                                </Card>
                            </Col>
                        </Row>

                    )) : (
                        <div className="center-align container">
                            <p className="Clique-na-busca-para center-align container">
                                Clique na busca para iniciar.
                        </p>
                        </div>
                    )
                }
            </div>
        </div>
    )
}


export default Home