import { all } from 'redux-saga/effects';
import listnerLogin from './login';
import listnerSearch from './search';

export default function* rootSaga() {
    yield all([
        listnerLogin(),
        listnerSearch()
    ])
}