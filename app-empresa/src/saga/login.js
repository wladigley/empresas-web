import axios from 'axios';
import { LOAD_MODAL} from '../actions/load-modal';
import { takeEvery, put } from 'redux-saga/effects';
import { URL_API_ENTERPRISE } from '../actions/comun';
import { LOAD_INFO_MODAL} from '../actions/info-modal';
import { FETCH_LOGIN, FETCH_SUCESS_LOGIN, FETCH_ERROR_LOGIN } from '../actions/login';

function* doLogin(action) {
    let result = {
        success: false,
        error: {},
        data: {}
    }
    let token = {
        AccessToken:'',
        client:'',
        uid:''
    }
    let login = {
        email: action.payload.email,
        password: action.payload.password
    }
    const json = JSON.stringify(login);

    yield put({ type: LOAD_MODAL,payload:true });;

    yield axios.post(URL_API_ENTERPRISE+'/users/auth/sign_in',
        json,
        {
            headers: {
                'Content-Type': 'application/json',
                "Cache-Control": "no-cache",
                'Accept': '*/*',
            }
        })
        .then(response => {
            if (response.data.success === true) {
                result.success = true;
                result.data = response.data;

                token.AccessToken = response.headers["access-token"];
                token.client = response.headers.client;
                token.uid = response.headers.uid;
                result.data = {...result.data, token}
            }
            else {
                result.success = false;
                result.error = {};
            }
        })
        .catch(error => {
            result.success = false;
            result.error = error;
        });

    yield put({ type: LOAD_MODAL,payload:false });;
    if (result.success) {
        yield put({ type: FETCH_SUCESS_LOGIN, payload: result.data });
    } else{
        yield put({ type: FETCH_ERROR_LOGIN, payload: result.error });
        yield put({ type: LOAD_INFO_MODAL,payload:{  show: true, txt: 'Falha durante o Login, tente novamente.'}});
    }
}
export default function* listnerLogin() {
    yield takeEvery(FETCH_LOGIN, doLogin);
}