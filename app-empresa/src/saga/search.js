import { takeEvery, put, select } from 'redux-saga/effects';
import { getToken } from '../redux/selectors';
import axios from 'axios';
import { LOAD_MODAL } from '../actions/load-modal';
import { URL_API_ENTERPRISE } from '../actions/comun';
import { LOAD_INFO_MODAL } from '../actions/info-modal';
import { FETCH_SEARCH } from '../actions/search';
import { RESULT_ENTERPRISE } from '../actions/home';
import { FETCH_SUCCESS_DETAIL } from '../actions/detail';

function* fetchSearch(action) {

    // Tipos de retornos para o payload
    // data ------------------------------> informação da pesquisa
    // requestData.allResult -------------> pesquisa todas as empresas
    // requestData.singleResult ----------> pesquisa empresa por id
    // requestData.dinamycResult ---------> pesquisa empresa por type e id
    // requestData.actionSuccesResult ----> Action para sucesso no retoro da pesquisa
    const requestData = action.payload; // Recupera as informações da pesquisa pelo payload
    let url = '';
    //condição para cada tipo de pesquisa
    if (requestData.allResult) {
        url = '/enterprises';

    } else if (requestData.singleResult) {
        url = '/enterprises/' + requestData.data;

    } else if (requestData.dinamycResult) {
        url = '/enterprises?enterprise_types=' + requestData.type + '&name=' + requestData.data;
    }

    let result = {
        success: false,
        error: {},
        data: {}
    }

    let token = yield select(getToken);
    let headers = {
        'Content-Type': 'application/json',
        'access-token': token.AccessToken,
        'client': token.client,
        'uid': token.uid
    }

    yield put({ type: LOAD_MODAL, payload: true });;

    axios.interceptors.request.use(function (config) {
        config.headers = headers;
        return config;
    });

    console.log(URL_API_ENTERPRISE + url)
    yield axios.get(URL_API_ENTERPRISE + url)
        .then(response => {
            if (response.status === 200) {
                result.success = true;
                result.data = response.data;
            }
            else {
                result.success = false;
                result.error = response.error;
            }
        })
        .catch(error => {
            result.success = false;
            result.error = error;
        });

    yield put({ type: LOAD_MODAL, payload: false });
    if (result.success) {
        if (requestData.actionSuccesResult === RESULT_ENTERPRISE) {

            yield put({ type: RESULT_ENTERPRISE, payload: { enterprises: result.data.enterprises } });

        } else if (requestData.actionSuccesResult === FETCH_SUCCESS_DETAIL) {
            yield put({ type: FETCH_SUCCESS_DETAIL, payload: result.data });

        }

    } else {
        yield put({ type: LOAD_INFO_MODAL, payload: { show: true, txt: 'Falha ao retornar pesquisa, tente novamente.' } });
    }

}

export default function* listnerSearch() {
    yield takeEvery(FETCH_SEARCH, fetchSearch);
}