export const FETCH_DETAIL = 'FETCH_DETAIL';
export const FETCH_SUCCESS_DETAIL = 'FETCH_SUCCESS_DETAIL';

export function getDetailEnterprise(text) {
    return { type: FETCH_DETAIL, payload: text }
}