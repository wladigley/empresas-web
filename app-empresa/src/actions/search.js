export const FETCH_SEARCH = 'FETCH_SEARCH';
export const FETCH_SUCCESS_SEARCH = 'FETCH_SUCCESS_SEARCH';
export const FETCH_ERROR_SEARCH = 'FETCH_ERROR_SEARCH';

export function exeuteSearch(data) {
    return { type: FETCH_SEARCH, payload: data }
}