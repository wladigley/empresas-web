import { combineReducers } from "redux";
import loginAutheticate from './login';
import loadModal from './load-modal';
import InfoModalOpen from './info-modal';
import loadHomeEnterprise from './home';
import loadDetailEnterprise from './detail';

const rootReducers = combineReducers({
    load: loadModal,
    login: loginAutheticate,
    infoModal: InfoModalOpen,
    enterprises: loadHomeEnterprise,
    detail: loadDetailEnterprise
});

export default rootReducers;