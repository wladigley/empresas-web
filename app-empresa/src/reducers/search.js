import { FETCH_SUCCESS_SEARCH,FETCH_ERROR_SEARCH } from '../actions/search'

const initialSearchState = 
{
    success:false,
    data:{}
}
 const resultSearch = (state = initialSearchState,action) => {
    switch (action.type) {
        case FETCH_SUCCESS_SEARCH:
            return{
                ...state,
                success:true,
                data: action.payload
            }
        case FETCH_ERROR_SEARCH:
            return{
                ...state,
                success:false,
                data: action.payload
            }
        default:
            return state
    }
}

export default resultSearch;