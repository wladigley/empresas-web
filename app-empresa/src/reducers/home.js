import { RESULT_ENTERPRISE } from '../actions/home'

const initialHomeState =
{
    enterprises: []
}
const loadHomeEnterprise = (state = initialHomeState, action) => {
    switch (action.type) {
        case RESULT_ENTERPRISE:
            window.location.hash = '#/home'
            return {
                enterprises: action.payload
            }
        default:
            return state
    }
}

export default loadHomeEnterprise;