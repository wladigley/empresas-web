import { FETCH_SUCCESS_DETAIL } from '../actions/detail'

const initiaDetailState =
{
    enterprises: {}
}
const loadDetailEnterprise = (state = initiaDetailState, action) => {
    switch (action.type) {
        case FETCH_SUCCESS_DETAIL:
            window.location.hash = '#/Detail'
            return {
                enterprises: action.payload
            }
        default:
            return state
    }
}

export default loadDetailEnterprise;