import { LOAD_INFO_MODAL } from '../actions/info-modal'

const initialInfoModalState =
{
    show: false,
    txt: ''
}
const InfoModalOpen = (state = initialInfoModalState, action) => {
    switch (action.type) {
        case LOAD_INFO_MODAL:
            return {
                show: action.payload.show,
                txt: action.payload.txt
            }
        default:
            return state
    }
}

export default InfoModalOpen;