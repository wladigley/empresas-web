
import { LOAD_MODAL } from '../actions/load-modal'

const initialLoadingState =
{
    loading: false
}
const loadModal = (state = initialLoadingState, action) => {
    switch (action.type) {
        case LOAD_MODAL:
            return {
                loading: action.payload
            }
        default:
            return state
    }
}

export default loadModal;